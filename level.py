# Level object stores 1-4 decisions, background image, and text prompts
class Level:

    def __init__(self, decisions, background, prompts):
        self.decisions = decisions
        self.background = background
        self.prompt = prompts
