from decision import Decision
from level import Level

# Returns the first level key and a tuple of tuples containing each level object and corresponding key
def build_levels(char):
    first_level = 1
    if char == "ENGINEER":
        # Dictionary of engineer level object data
        eng = {}
        # level_name[key] = {  dictionary data
        eng[1] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [Decision(2, "Continue")],
            'prompts':
                ["(A.I Assistant): The ship has sustained critical damages to the engine and communications hardware",
                 "(Weapon Specialist): We need to find some parts to fix up this ship!",
                 "(Engineer): Me and (Weapon Specialist) will search for some replacement parts, you two look for supplies."]
        }

        eng[2] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [Decision(3, "Ask (Weapons Specialist) to make an entry point")],
            'prompts':
                ["(Weapon Specialist): Looks like someone else crashed down here.",
                 "(Engineer): I wonder when this happened"],
        }

        eng[3] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(4, "Ask (Weapons Specialist) to open door."),
                Decision(5, "Force the door open"),
                Decision(6, "Shoot the door")
            ],
            'prompts':
                ["(Engineer): Can you find us a way inside?",
                 "(Weapon Specialist): Ok, lets see if we can find anything useful."]
        }
        eng[4] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(5, "Force the door open"),
                Decision(6, "Shoot the door")
            ],
            'prompts':
                ["(Weapon Specialist): The door is jammed shut. What should we try next?"]
        }
        eng[5] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(7, "Straight"),  # parts
                Decision(8, "Right")      # guard post - cockpit
            ],
            'prompts':
                ["--With tremendous pushing power the door swings open--",
                "(Weapon Specialist): Which way should we go?",
                " (Engineer): Hmm.."]
        }
        eng[6] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(5, "Force the door open"),
            ],
            'prompts':
                ["(Weapon Specialist): I don' think shooting the door will help.",
                 "(Engineer): Also have to remember we have limited ammo / supplies."]
        }
        eng[7] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(8, "Head back to the entrance to search the right path.")
            ],
            'prompts':
                ["(Weapon Specialist): Look there are some ship parts!",
                "(Engineer): We should continue searching for more."]
        }
        eng[8] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(9, "Sneak by"),
                Decision(14, "Attack"),
                Decision(20, "Approach peacefully")  # ends game
            ],
            'prompts':
                ["(Weapon Specialist): Looks like there's an alien gaurd post ahead.",
                "(Engineer): How do you want to handle this?"]
        }
        eng[9] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(10, "Head to the cockpit"),
                Decision(11, "Turn around and attack")
            ],
            'prompts':
                ["(Weapon Specialist): Seems like we were able to successfully sneak by",
                "(Engineer): Looks like the ships cock pit is up ahead",
                "(Weapon Specialist): Theres got to be parts that we can use there"]
        }
        eng[10] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(12, "Take the ship"),
                Decision(13, "Dismantle and bring parts back")
            ],
            'prompts':
                ["(Engineer): Wow! Nothing seems to be damaged here.",
                "(Weapon Specialist): Do you think we could just use this ship instead?",
                "(Engineer): Seems like it, everything looks like it will work."]
        }
        eng[11] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(10, "Head to the cockpit")
            ],
            'prompts':
                ["(Weapon Specialist): Don't think its too good of an idea to attack right now.",
                "(Engineer): Right, let's go check out the cockpit first then."]
        }
        eng[12] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(14, "Attack")
            ],
            'prompts':
                ["(Weapon Specialist): First we still need to deal with that alien gaurd post.",
                "(Engineer): Let's go do that then."]
        }
        eng[13] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(14, "Attack")
            ],
            'prompts':
                ["(Weapon Specialist): Wouldn't it just be easier to take this ship though?.",
                "(Engineer): Yeah, I guess it would.",
                "(Weapon Specialist): We'll just have to deal with that alien gaurd post.",
                "(Engineer): Let's go do that then."]
        }
        eng[14] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(15, "Rush in"),
                Decision(21, "Snipe from a distance") # end game
            ],
            'prompts':
                ["(Weapon Specialist): What should we do?"]
        }
        eng[15] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(16, "Head to the cockpit"),
                Decision(17, "Tell other crew mates")
            ],
            'prompts':
                ["-- The Engineer and Weapon Specialist run into attack --",
                "-- The aliens were not prepared and you take them by suprise --",
                "(Weapon Specialist): Easy enough, what now?"]
        }
        eng[16] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(17, "Go and get the other crew mates"),
            ],
            'prompts':
                ["(Weapon Specialist): Shouldn't we get our other crew mates first?",
                "(Engineer): Oh yeah, true."]
        }
        eng[17] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision('win', "Fly home")
            ],
            'prompts':
                ["-- The Engineer and Weapon Specialist tell the crewmates --",
                "(Medic): That sounds like a great idea!",
                "(AI Assistant): That is a valid plan.",
                "-- The crew heads back to the other ship --"]
        }
        eng['win'] = { #NEED TO END THE GAME = success
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
            ],
            'prompts':
                ["-- The crew successfully flies the ship home --"]
        }
        eng[20] = { #NEED TO END THE GAME = Fail
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(20, "END GAME", 100, 100),
            ],
            'prompts':
                ["-- The Engineer and Weapon Specialist attempt to sneak by --",
                "-- The aliens spotted them and killed them on the spot --"]
        }
        eng[21] = { #NEED TO END THE GAME = Fail
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(20, "END GAME", 100, 100),
            ],
            'prompts':
                ["-- The Engineer and Weapon Specialist attempt to snipe from a distance --",
                "-- They both miss but the aliens don't... --"]
        }
        # return first_level, and tuple of tulpes of: ITERATE - for each key/value in every dictionary:
        # {(current level key: new Level Object(pass values)), (k:Level2), (k:Level3)...}
        return first_level, {k: Level(**v) for k, v in eng.items()}

    elif char == "SCIENTIST":
        sci = {}

        sci[1] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [Decision(2, "Continue")],
            'prompts':
                ["(A.I Assistant): The ship has sustained critical damages to the engine and communications hardware",
                 "(Weapon Specialist): We need to find some parts to fix up this ship!",
                 "(Scientist): (Engineer) and (Weapon Specialist), go and search for some replacement parts, while me and (A.I assistant) look for supplies.",
                 "(Engineer): Sounds good we'll let you know if we find anything, keep your radio pods on so we can stay in communication with you guys"]
        }
        sci[2] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(3, "Explore surroundings to learn more about the area"),
                Decision(4, "Hold off on research and search for supplies")
            ],
            'prompts':
                ["(Scientist): We still have some supplies here left on the ship. A.I, how long will our current rations last us?",
                "(A.I Assistant): Current rations should last approximately 4 days",
                "(Scientist): I see, so I do have a bit of time on my hand. Should I explore this place and get new data for research or should I look for supplies now?"
                ]

        }
        sci[3] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(5, "Take the gems and return to the ship"),
                Decision(6, "Take the gems and explore the cave.")
            ],
            'prompts':
                ["(Scientist): A.I, keep watch of the ship while I'm away, I shouldn't be gone for long",
                "-- As the scientist ventures off into the uncharted land, documenting every strange item he encounters, he stumbles upon a cave riddled with strange orange gems. --",
                "(Scientist): These gems seem to be emitting a strange energy, definitely something I should bring back to the ship for further research. I also, shouldn't go in this cave alone,but it might be a wasted opportunity"
                ]

        }
        sci[4] = {
            'background': "assets/backgrounds/engineer/eng1.jpg",
            'decisions': [
                Decision(3, "Explore surroundings to learn more about the area"),
                Decision(4, "Hold off on research and search for supplies")
            ],
            'prompts':
                ["(Scientist): We still have some supplies here left on the ship. A.I, how long will our current rations last us?",
                "(A.I Assistant): Current rations should last approximately 4 days",
                "(Scientist): I see, so I do have a bit of time on my hand. Should I explore this place and get new data for research or should I look for supplies now?"
                "(Scientist): I see, so I do have a bit of time on my hand. Should I explore this place and get new data for research or should I look for supplies now?"
                ]

        }
        return first_level, {k: Level(**v) for k, v in sci.items()}