# Xplorε
A text based space adventure game. Make the right decisions to keep your crew safe and to voyage home.
Currently game_data.py only contains 1 level and character, but this game has all the programming to support multiple levels and characters, as well as dynamic decision based storylines. 

## Requirements:
- [Python 3.7](https://www.python.org/downloads/) (tested on 3.7, may work on Python 3.6.1+)
- [Pygame](https://www.pygame.org/wiki/GettingStarted) 
- Pygame-menu install: `pip install pygame-menu`

Run main.py to play: `python main.py`

## Main Menu
The game will start at the main menu. 
- **New Game** will bring you to another menu where you can start the game or select what character you want to play by using the left and right arrow keys on your keyboard. The back option will take you back to the main menu. 
- **Load Game** will have a selection of game saves to resume. 
- **Quit** will close the application.

## How to Play
Selecting **New Game** then **Start** will start the game at the first level. Text will appear with at the start of each level and you can press enter to cycle the next text prompt. Choices will then appear on the bottom. Press keys 1-3 corresponding to the choice that you want to select, moving you onto the next level. The escape key will pause the game, prompting you to put your save name and return you to the main menu.

## UML Diagrams

### Main Menu
![diagram: main_menu_uml.png](./assets/uml/main_menu_uml.png)

### Character Selection
![diagram: customization_screen_uml.png](./assets/uml/customization_screen_uml.png)

### Gameplay
![diagram: gameplay_uml.png](./assets/uml/gameplay_uml.png)

### Program State
![diagram: program_state_uml.png](./assets/uml/program_state_uml.png)

