# Import pygame and libraries
import json
from pygame.locals import *
import os
import pygame
import game
import pygame_textinput


# Import pygameMenu
import pygameMenu
from pygameMenu.locals import *

# Globals used throughout the menu
ABOUT = [PYGAMEMENU_TEXT_NEWLINE,
         'Author: {0}'.format('Austin Agronick'),
         PYGAMEMENU_TEXT_NEWLINE,
         'Publisher Name: {0}'.format('SpaceGuy')]
Leaderboard = ['Leaderboard:']
COLOR_BACKGROUND = (25, 25, 25)
COLOR_BLACK = (0, 0, 0)
COLOR_WHITE = (255, 255, 255)
FPS = 60.0
MENU_BACKGROUND_COLOR = (0, 255, 255)
WINDOW_SIZE = (640, 480)

# -----------------------------------------------------------------------------
# Init pygame
pygame.init()
os.environ['SDL_VIDEO_CENTERED'] = '1'

# Create pygame screen and objects
surface = pygame.display.set_mode(WINDOW_SIZE)
pygame.display.set_caption('Xplore')
clock = pygame.time.Clock()
dt = 1 / FPS
music = pygame.mixer.music.load('sounds/carpe_diem.mp3')
pygame.mixer.music.play(-1)  # Play music and loop indefinitely

# Global variables for character and game save selection
CHAR_SELECT = ['ENGINEER']
SAVE_SELECT = ['']

# -----------------------------------------------------------------------------

# Changes players' character at new game screen
def change_character(d):
    print('Selected character: {0}'.format(d))
    CHAR_SELECT[0] = d

# Changes save at load game screen
def change_save(d):
    print('Selected save: {0}'.format(d))
    SAVE_SELECT[0] = d

# Fade effect transitioning from menu to gameplay
def fade(width, height, background, text, text_width):
    fade = pygame.Surface((width, height))  # Create fade surface
    fade.fill((0, 128, 0))
    # Fade in
    for alpha in range(0, 300):
        fade.set_alpha(alpha)
        if alpha > 50: fade.blit(text, ((WINDOW_SIZE[0] - text_width) / 2, WINDOW_SIZE[1] / 2)) # Draw 'Playing as' text
        surface.blit(fade, (0, 0))  # Blit fade onto surface
        pygame.display.flip()  # Update display
        pygame.time.delay(4)

    # Fade out
    for alpha in reversed(range(0, 300)):
        fade.set_alpha(alpha)
        pygame.draw.rect(surface, COLOR_BLACK, (0, WINDOW_SIZE[1]-150, 640, 150), 0)
        surface.blit(pygame.image.load(background), (0, 0))
        fade.blit(text, ((WINDOW_SIZE[0] - text_width) / 2, WINDOW_SIZE[1] / 2))  # Draw 'Playing as' text
        surface.blit(fade, (0, 0))  # Blit fade onto surface
        pygame.display.flip()  # Update display
        #pygame.time.delay(1)-


# Start a new game, takes character selection, a game save, and font as parameters
def new_game_function(CHAR_SELECT, saves, font):
    if saves:  # if loading from save
        try:  # load selected save
            level = saves[SAVE_SELECT[0]]['level']
        except:  # default save (first save)
            level = saves[save_names[0][0]]['level']
    else:  # new game
        level = 1

    CHAR_SELECT = CHAR_SELECT[0]
    assert isinstance(CHAR_SELECT, str)

    # Create text render, background image, and player object for selected character
    if CHAR_SELECT == 'ENGINEER':
        f = font.render('Playing as Engineer', 1, COLOR_WHITE)
        background = "assets/backgrounds/engineer/eng1.jpg"
        player = game.Player(CHAR_SELECT, intelligence=10, strength=10, health=100, current_level=level, score=0,
                             surface=surface, main_menu=main_menu, clock=clock)
    elif CHAR_SELECT == 'SPECIALIST':
        f = font.render('Playing as Weapons Specialist', 1, COLOR_WHITE)
        background = "assets/backgrounds/engineer/eng1.jpg"
        player = game.Player(CHAR_SELECT, intelligence=10, strength=10, health=100, current_level=level, score=0,
                             surface=surface, main_menu=main_menu, clock=clock)
    elif CHAR_SELECT == 'SCIENTIST':
        f = font.render('Playing as Scientist', 1, COLOR_WHITE)
        background = "assets/backgrounds/engineer/eng1.jpg"
        player = game.Player(CHAR_SELECT, intelligence=10, strength=10, health=100, current_level=level, score=0,
                             surface=surface, main_menu=main_menu, clock=clock)
    else:
        raise Exception('Unknown CHAR_SELECT {0}'.format(CHAR_SELECT))


    # Reset main menu and disable
    main_menu.disable()
    main_menu.reset(1)

    # Fade to gameplay
    f_width = f.get_size()[0]
    fade(WINDOW_SIZE[0], WINDOW_SIZE[1], background, f, f_width)

    # Start gameplay
    try:
        player.start_game()
    except game.ExitGameException as e: # automatically prompt for game save on exit
        save_name = user_input()
        surface.fill(COLOR_WHITE)
        message = pygame.font.SysFont('Arial', 16).render("Progress saved!", True, (0, 0, 0))
        surface.blit(message, (WINDOW_SIZE[0] // 2 - 150, (WINDOW_SIZE[1] // 2) + 50))
        pygame.display.flip()

        try:  # If save file exits and isn't empty
            with open('game.save', "r") as savefile:
                saves = json.loads(savefile.read())
        except:
            saves = {}  # Initialize a saves dictionary
        # Add new save to dictionary
        saves[save_name] = {
            'name': e.player.name,
            'level': e.player.current_level
        }
        # Dump saves dictionary to file
        with open('game.save', "w+") as savefile:
            savefile.write(json.dumps(saves))
        exit()

    # If player has won, congratulate and return to main menu
    except game.WonGameException as e:
        main_menu.enable()
        main_menu.reset(1)
        surface.fill(COLOR_WHITE)
        message = pygame.font.SysFont('Arial', 20).render("YOU WON!", True, (0, 0, 0))
        surface.blit(message, (WINDOW_SIZE[0] // 2 - 25, (WINDOW_SIZE[1] // 2)))
        pygame.display.flip()
        pygame.time.delay(5000)

    # If player has lost
    except game.LostGameException as e:
        pass
    # Show main menu


# Function used by menus, draw on background while menu is active
def main_background():
    surface.blit(pygame.image.load("assets/bg.jpg"), (0, 0))


# -----------------------------------------------------------------------------
# lOAD GAME MENU
load_game_menu = pygameMenu.Menu(surface,
                                 bgfun=main_background,
                                 color_selected=COLOR_WHITE,
                                 font=pygameMenu.fonts.FONT_NEVIS,
                                 font_color=COLOR_BLACK,
                                 font_size=25,
                                 menu_alpha=50,
                                 menu_color=MENU_BACKGROUND_COLOR,
                                 menu_height=int(WINDOW_SIZE[1] * 0.7),
                                 menu_width=int(WINDOW_SIZE[0] * 0.7),
                                 onclose=PYGAME_MENU_DISABLE_CLOSE,
                                 option_shadow=False,
                                 title='Load Game',
                                 window_height=WINDOW_SIZE[1],
                                 window_width=WINDOW_SIZE[0]
                                 )

try:  # If save file exits and isn't empty
    with open('game.save', "r") as savefile:
        saves = json.loads(savefile.read())
    save_names = [(i[0], i[0]) for i in list(zip(tuple(saves.keys())))] # get list of game save dictionary keys
    # onchange will set selected save's key = SAVE_SELECT
    load_game_menu.add_selector('Select Save', save_names, onreturn=None, onchange=change_save)
    load_game_menu.add_option('Load Save', new_game_function, CHAR_SELECT, saves,
                              pygame.font.Font(pygameMenu.fonts.FONT_NEVIS, 30))
    load_game_menu.add_option('Back', PYGAME_MENU_BACK)
except: # No saves
    load_game_menu.add_option('No Saves Available', PYGAME_MENU_BACK)

# NEW GAME MENU
new_game_menu = pygameMenu.Menu(surface,
                                bgfun=main_background,
                                color_selected=COLOR_WHITE,
                                font=pygameMenu.fonts.FONT_NEVIS,
                                font_color=COLOR_BLACK,
                                font_size=25,
                                menu_alpha=50,
                                menu_color=MENU_BACKGROUND_COLOR,
                                menu_height=int(WINDOW_SIZE[1] * 0.7),
                                menu_width=int(WINDOW_SIZE[0] * 0.7),
                                onclose=PYGAME_MENU_DISABLE_CLOSE,
                                option_shadow=False,
                                title='New Game',
                                window_height=WINDOW_SIZE[1],
                                window_width=WINDOW_SIZE[0]
                                )
# When pressing back -> play(CHAR_SELECT[0], font)
new_game_menu.add_option('Start', new_game_function, CHAR_SELECT, None,
                         pygame.font.Font(pygameMenu.fonts.FONT_NEVIS, 30))
new_game_menu.add_selector('Select Character', [('Engineer', 'ENGINEER'),
                                                ('Specialist', 'SPECIALIST'),
                                                ('Scientist', 'SCIENTIST')],
                           onreturn=None,
                           onchange=change_character)
new_game_menu.add_option('Back', PYGAME_MENU_BACK)

# ABOUT MENU
about_menu = pygameMenu.TextMenu(surface,
                                 bgfun=main_background,
                                 color_selected=COLOR_WHITE,
                                 font=pygameMenu.fonts.FONT_NEVIS,
                                 font_color=COLOR_BLACK,
                                 font_size_title=30,
                                 font_title=pygameMenu.fonts.FONT_NEVIS,
                                 menu_alpha=50,
                                 menu_color=MENU_BACKGROUND_COLOR,
                                 menu_color_title=COLOR_WHITE,
                                 menu_height=int(WINDOW_SIZE[1] * 0.7),
                                 menu_width=int(WINDOW_SIZE[0] * 0.7),
                                 onclose=PYGAME_MENU_DISABLE_CLOSE,
                                 option_shadow=False,
                                 text_color=COLOR_BLACK,
                                 text_fontsize=20,
                                 title='About',
                                 window_height=WINDOW_SIZE[1],
                                 window_width=WINDOW_SIZE[0]
                                 )
for m in ABOUT:
    about_menu.add_line(m)
about_menu.add_line(PYGAMEMENU_TEXT_NEWLINE)
about_menu.add_option('Back', PYGAME_MENU_BACK)

# MAIN MENU
main_menu = pygameMenu.Menu(surface,
                            bgfun=main_background,
                            color_selected=COLOR_WHITE,
                            font=pygameMenu.fonts.FONT_BEBAS,
                            font_color=COLOR_BLACK,
                            font_size=25,
                            # transparency value (100 is non transparent)
                            menu_alpha=75,
                            menu_color=MENU_BACKGROUND_COLOR,
                            menu_height=int(WINDOW_SIZE[1] * 0.7),
                            menu_width=int(WINDOW_SIZE[0] * 0.7),
                            onclose=PYGAME_MENU_DISABLE_CLOSE,
                            option_shadow=False,
                            title='Xplore',
                            window_height=WINDOW_SIZE[1],
                            window_width=WINDOW_SIZE[0]
                            )
main_menu.add_option('New   Game', new_game_menu)
main_menu.add_option('Load   Game', load_game_menu)
main_menu.add_option('About', about_menu)
main_menu.add_option('Quit', PYGAME_MENU_EXIT)

# Prompts user for input and returns a string. See \assets\pygame-text-input-master for details
def user_input():
    pygame.display.set_caption('Xplore - Save Game')
    textinput = pygame_textinput.TextInput()

    while True:
        surface.fill(COLOR_WHITE)
        textsurface = pygame.font.SysFont('Arial', 15).render("Enter your name and press Enter/Return to save your "
                                                              "progress.", True,
                                                              (0, 0,
                                                                                                                0))
        surface.blit(textsurface, (WINDOW_SIZE[0]//2 - 150, (WINDOW_SIZE[1]//2) + 50))

        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()

        # Feed it with events every frame, return when return key is pressed
        if textinput.update(events):
            return textinput.get_text()
        # Blit its surface onto the screen
        surface.blit(textinput.get_surface(), (WINDOW_SIZE[0]//2 - 100, WINDOW_SIZE[1]//2))
        pygame.display.update()

# -----------------------------------------------------------------------------
# Main loop
while True:

    # Tick
    clock.tick(60)

    # Application events
    events = pygame.event.get()
    for event in events:
        if event.type == QUIT:
            exit()

    # Main menu
    main_menu.mainloop(events)

    # Flip surface
    pygame.display.flip()
