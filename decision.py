# Stores the decision's players will make to progress through the game
# next_level - level to be played if this decision is selected
# answer - the text that will be displayed at gameplay for the player to select
# intelligence/strength - mostly nonfunctional, (is needed for skill based decision logic - unimplemented)
class Decision:
    def __init__(self, next_level, answer, intelligence=0, strength=0):
        self.answer = answer
        self.intelligence = intelligence
        self.strength = strength
        self.next_level = next_level # dictionary key

    def get_answer(self):
        return self.answer
