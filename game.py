from pygame.locals import *
import random
import pygame

from game_data import build_levels

WINDOW_SIZE = (640, 480)


class ExitGameException(Exception):

    def __init__(self, player=None):
        self.player = player


class WonGameException(Exception):

    def __init__(self, player=None):
        self.player = player


class LostGameException(Exception):

    def __init__(self, player=None):
        self.player = player


# Player class store all game specific data and manages level transition
class Player:

    def __init__(self, name, intelligence, strength, health=100, current_level=1, score=0, surface=None, main_menu=None,
                 clock=None):
        self.name = name
        self.intelligence = intelligence
        self.strength = strength
        self.health = health
        self.score = score
        self.surface = surface

        self.main_menu = main_menu
        self.clock = clock

        self.first_level, self.levels = build_levels(name)
        self.current_level = current_level

    def start_game(self):
        self.load_level(self.current_level)

    # Loads level and creates level object
    def load_level(self, current_level):
        self.current_level = current_level
        level_obj = self.levels[current_level]

        # Draw level background
        def draw_bg():
            pygame.draw.rect(self.surface, (0, 0, 0), (0, WINDOW_SIZE[1] - 150, 640, 150), 0)
            self.surface.blit(pygame.image.load(level_obj.background), (0, 0))

        # Display level prompt
        for prompt in level_obj.prompt:
            draw_bg()
            textsurface = pygame.font.SysFont('Arial', 15).render(prompt, True, (255, 255, 255))
            self.surface.blit(textsurface, (10, 335))
            pygame.display.flip()
            while self.run_game_loop().type != KEYDOWN:  # Press any key to cycle next prompt
                    pass

        # Display 1-4 level decisions
        width = 150
        length = 90
        y = 360
        margin = (640 - (width * 4)) // 5
        positions = (
            (margin, y, width, length),
            (margin + width + margin, y, width, length),
            (margin + width + margin + width + margin, y, width, length),
            (margin + width + margin + width + margin + width + margin, y, width, length)
        )

        # Draw decisions
        # TODO Display decisions in random order
        for i, decision in enumerate(level_obj.decisions):
            rect = pygame.draw.rect(self.surface, (47, 79, 79), positions[i])
            # TODO try/catch if decision text is outside wrap
            draw_text(self.surface, decision.get_answer(), (255, 255, 255), rect, pygame.font.SysFont('Arial', 14),
                      aa=True, bkg=None)
        pygame.display.flip()

        # Process user input
        selected_choice = None
        # Each key (1,2,3,4) is mapped to a decision
        key_answer_map = (K_1, K_2, K_3, K_4)
        key_answer_map = key_answer_map[:len(level_obj.decisions)]
        while selected_choice is None:
            event = self.run_game_loop()

            if event.type == KEYDOWN:
                if event.key in key_answer_map:
                    selected_choice = level_obj.decisions[key_answer_map.index(event.key)] # Valid choice selected
                else:
                    # Invalid key input
                    text_surface = pygame.font.SysFont('Arial', 15).render(
                        "Press key 1, 2, 3, or 4 to select a decision.", True, (255, 255, 255))
                    self.surface.blit(text_surface, (10, 455))
                    pygame.display.flip()

        # Process the decision
        if self.process_choice(selected_choice):
            result = selected_choice.next_level
            # Game won
            if result == 'win':
                raise WonGameException(self)
            else:
                # Next level
                self.load_level(result)

        else:
            # Game lost
            raise LostGameException(self)

    # Calculate choice probability and adjust health/score as needed (feature not fully implemented in current build)
    def process_choice(self, selected_choice):
        # If player has required intelligence
        if selected_choice.strength == 0 and self.intelligence > selected_choice.intelligence:
            return True
        # If player does not have required intelligence
        elif self.intelligence < selected_choice.intelligence:
            percentage = (self.intelligence/selected_choice.intelligence)
        # If player has required strength
        if selected_choice.intelligence == 0 and self.strength > selected_choice.strength:
            return True
        # If player does not have required strength
        elif self.strength < selected_choice.strength:
                percentage = (self.strength/selected_choice.strength)
        if random.randrange(100) < percentage*100:
            return True

        return False

    def run_game_loop(self):

        while True:
            # Clock tick
            self.clock.tick(60)

            # Application events
            playevents = pygame.event.get()

            for e in playevents:
                if e.type == KEYDOWN:
                    if e.key == K_ESCAPE and self.main_menu.is_disabled():
                        raise ExitGameException(self)

                return e


# draw some text into an area of a surface
# automatically wraps words inside of rect
# can apply text color, font, anti aliasing, background color
# returns any text that didn't get blitted
def draw_text(surface, text, color, rect, font, aa=False, bkg=None):
    rect = Rect(rect)
    y = rect.top
    lineSpacing = -2

    # get the height of the font
    fontHeight = font.size("Tg")[1]

    while text:
        i = 1

        # determine if the row of text will be outside our area
        if y + fontHeight > rect.bottom:
            break

        # determine maximum width of line
        while font.size(text[:i])[0] < rect.width and i < len(text):
            i += 1

        # if we've wrapped the text, then adjust the wrap to the last word
        if i < len(text):
            i = text.rfind(" ", 0, i) + 1

        # render the line and blit it to the surface
        if bkg:
            image = font.render(text[:i], 1, color, bkg)
            image.set_colorkey(bkg)
        else:
            image = font.render(text[:i], aa, color)

        surface.blit(image, (rect.left, y))
        y += fontHeight + lineSpacing

        # remove the text we just blitted
        text = text[i:]

    return text